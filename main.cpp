#include "utility.h"

#include <cstdlib>
#include <cstdio>
#include <ctime>


int main( int argc, char **argv ){
	int N = atoi(argv[1]) ;

	uint32_t *votes ;
	uint32_t *answer ;

	// Get Memory
	get_memory( &answer, &votes, N ) ;

	// Generate Votes
	fprintf(stderr, "Generating Votes...") ;
	generate_votes( votes, N ) ;
	fprintf(stderr, " Done\n") ;

	// Get Answer
	auto time_begin = clock() ;
	get_answer( answer, votes, N ) ;
	double duration = (clock()-time_begin)/(double)CLOCKS_PER_SEC ;

	fprintf(stderr, "Duration: %.3fs\n", duration) ;

	// Output
	if( N <= 10 )
		print_votes( votes, N ) ;
	print_answer( answer ) ;

	return 0 ;
}

