#ifndef __UTILITY_H__
#define __UTILITY_H__

#define CANDIDATES	32

#include <cinttypes>

//// Shared Functions ////
void generate_votes(uint32_t *votes, int N) ;

void print_votes(const uint32_t *votes, int N) ;

void print_answer(const uint32_t *answer) ;
//////////////////////////

//// CPU/GPU Codes ////
void get_memory(uint32_t **answer, uint32_t **votes, int N) ;

void get_answer(uint32_t *answer, const uint32_t *votes, int N) ;
///////////////////////

#endif

