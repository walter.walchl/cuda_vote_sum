# CPU
g++ main.cpp utility.cpp cpu_solver.cpp -o cpu

# GPU
nvcc main.cpp utility.cpp gpu_solver.cu -o gpu