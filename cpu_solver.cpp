#include "utility.h"

void get_memory(uint32_t **answer, uint32_t **votes, int N){
	*votes = new uint32_t[N] ;
	*answer = new uint32_t [CANDIDATES] ;
}


void get_answer(uint32_t *answer, const uint32_t *votes, int N){
	for( int i=0 ; i<CANDIDATES ; i++ )
		answer[i] = 0 ;

	for( int i=0 ; i<N ; i++ ){
		for( int j=0 ; j<32 ; j++ ){
			if( (votes[i]>>j) & 1 )
				answer[j] ++ ;
		}
	}
}

