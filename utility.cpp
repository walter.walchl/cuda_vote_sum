#include "utility.h"

#include <cstdio>
#include <cstdlib>

#define FILENAME "votes.dat"


void generate_votes(uint32_t *votes, int N){
	FILE *fp = fopen( FILENAME, "rb" ) ;
	if( fp ){
		int SIZE ;
		fread( &SIZE, sizeof(int), 1, fp ) ;

		if( SIZE >= N ){
			fprintf( stderr, " Reading File..." ) ;
			fread( votes, sizeof(uint32_t), N, fp ) ;
			fclose( fp ) ;
		}
		else{
			fclose( fp ) ;
			fp = NULL ;
		}
	}

	if( !fp ){
		for( int i=0 ; i<N ; i++ )
			votes[i] = (rand()<<8)^(rand()>>16) ;

		fprintf( stderr, " Writing File..." ) ;
		fp = fopen( FILENAME, "wb" ) ;
		fwrite( &N, sizeof(int), 1, fp ) ;
		fwrite( votes, sizeof(uint32_t), N, fp ) ;
		fclose( fp ) ;
	}
}


void print_votes(const uint32_t *votes, int N){
	for( int i=0 ; i<N ; i++ )
		printf(" %08X", votes[i]) ;
	printf("\n") ;
}


void print_answer(const uint32_t *answer){
	for( int i=0 ; i<CANDIDATES ; i++ )
		printf("answer[%02d]: %9d\n", i, answer[i]) ;
}

